import flask
from flask import Flask, request, render_template, url_for, redirect
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime
from sqlalchemy import MetaData
app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///blog.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)
app.app_context().push()
metadata = MetaData()

class Article(db.Model):
    with app.app_context():
        id = db.Column(db.Integer, primary_key=True)
        tittle = db.Column(db.String(100), nullable=False)
        intro = db.Column(db.String(300), nullable=False)
        text = db.Column(db.Text, nullable=False)
        date = db.Column(db.DateTime, default=datetime.now().date())

        def __repr__(self):
            with app.app_context():
                return '<Article %r' % self.id


@app.route("/")
def hello():
    return render_template("index.html")

@app.route("/about")
def about():
    return render_template("about.html")

@app.route("/free")
def free():
    return "free"

@app.route("/pro")
def pro():
    return "pro"

@app.route("/enterprise")
def enterprise():
    return "enterprise"

@app.route("/create_article", methods=['POST', 'GET'])
def create_article():
    if request.method == 'POST':
        tittle = request.form['tittle']
        intro = request.form['intro']
        text = request.form['text']

        article = Article(tittle=tittle, intro=intro, text=text)
        
        try:
            db.session.add(article)
            db.session.commit()
            return redirect('/posts')
        except:
            return "при добавдении статьи произошла ошибка"
    else:
        return render_template("create_article.html")

@app.route("/posts", methods=['POST', 'GET'])
def posts():
    articles = Article.query.order_by(Article.date.desc()).all()
    return render_template("posts.html", articles=articles)

@app.route('/posts/<int:id>')
def post_detail(id):
    article = Article.query.get(id)
    return render_template("post_detail.html", article=article)

@app.route('/posts/<int:id>/del')
def post_delete(id):
    article = Article.query.get_or_404(id)

    try:
        db.session.delete(article)
        db.session.commit()
        return redirect('/posts')
    except:
        return "при удалении статьи произошла ошибка"   
    
@app.route("/posts/<int:id>/update", methods=['POST', 'GET'])
def post_update(id):
    article = Article.query.get(id)
    if request.method == 'POST':
        article.tittle = request.form['tittle']
        article.intro = request.form['intro']
        article.text = request.form['text']
   
        try:
            db.session.commit()
            return redirect('/posts')
        except:
            return "при редактировании статьи произошла ошибка"
    else:
        return render_template("post_update.html", article=article)

if __name__ == '__main__':
    app.run(debug=True,host = '0.0.0.0', port=3000) #
